
-- items

DROP TABLE IF EXISTS items;
CREATE TABLE IF NOT EXISTS 
items 
( 	
	item_id VARCHAR(10) PRIMARY KEY,
	item_description VARCHAR(100),
	item_basis_value DECIMAL NOT NULL,
	item_basis_unit VARCHAR(10) NOT NULL
	
);

INSERT INTO items VALUES("CIV001","WHITE WASHING 3 COATS",1000, "SFT");
INSERT INTO items VALUES("CIV002","SUPPLYING AND LAYING,FITTING & FIXING OF 9"" OR 225mm DIA",1, "RFT");
INSERT INTO items VALUES("CIV003","R.C.C.(1:2:4) IN SUNSHADE,CORNICE,RAILING,DROP WALLS FOR ALL FLOORS",100, "SFT");

select * from items;

-- costtypes
DROP TABLE IF EXISTS costtypes;
CREATE TABLE IF NOT EXISTS 
costtypes 
( 	
	costtype_id VARCHAR(10) PRIMARY KEY,
	costtype_type VARCHAR(20) NOT NULL
);

INSERT INTO costtypes VALUES("CIVMAT","CIVIL MATERIAL");
INSERT INTO costtypes VALUES("ELEMAT","ELECTRICAL MATERIAL");
INSERT INTO costtypes VALUES("AUXMAT","AUXILIARY MATERIAL");
INSERT INTO costtypes VALUES("LCS","LOCAL CARTAGE SUNDRIES:");
INSERT INTO costtypes VALUES("TOOLS","TOOLS PLANTS:");
INSERT INTO costtypes VALUES("LABOUR","LABOUR");

-- costlines
DROP TABLE IF EXISTS costlines;
CREATE TABLE IF NOT EXISTS 
costlines 
( 	
	costline_id VARCHAR(10) PRIMARY KEY,
	costtype_id VARCHAR(10) NOT NULL,
	costline_id_description VARCHAR(100),
	costline_id_cost DECIMAL NOT NULL,
	costline_id_basis_value DECIMAL NOT NULL,
	costline_id_basis_unit VARCHAR(10) NOT NULL,
	FOREIGN KEY (costtype_id) REFERENCES costtypes(costtype_id)
	
);

--CIV001
/*
CIV001

BASIC	COST OF MATERIALS:						 	 TK 
ML02	Stone Lime	20.00 	KG	@TK	5.00 	Per KG	 100.00 
ML32	Blue		0.50 	Kg	@TK	40.00 	Per Kg	 20.00 
ML31	Gum			0.50 	Kg	@TK	40.00 	Per Kg	 20.00 

AUXILIARY MATERIAL COST:						
SCAFFOLDING						 10.00 

LABOUR CHARGE:						
FOR WHITE WASH						 50.00 
*/
INSERT INTO costlines VALUES("ML02","CIVMAT","Stone Lime",1000,1,"KG");
INSERT INTO costlines VALUES("ML32","CIVMAT","Blue",40,1, "KG");
INSERT INTO costlines VALUES("ML31","CIVMAT","Gum",40,1, "KG");
INSERT INTO costlines VALUES("AM02","AUXMAT","SCAFFOLDING",10,1000, "SFT");
INSERT INTO costlines VALUES("L001","LABOUR","WHITE WASH",50,1000, "SFT");


/*
CIV002
SUPPLYING AND LAYING,FITTING & FIXING OF 9" OR 225mm DIA

BASIC COST OF MATERIALS:						 TK 
PI04	9" dia R.C.C pipe	1.00 	Rft	@TK	20.00 	Per rft	 20.00 

AUXILIARY MATERIAL COST:						
	One layer brick flat soling	2.08 	sft	@TK	10.00 	Per sft	 20.80 
	C.C. (1:3:6)	1.13 	cft	@TK	90.00 	Per cft	 101.70 
	Cost of earth work 	7.29 	cft	@TK	0.80 	Per cft	 5.83 
	Back filling of trench 	5.71 	cft	@TK	0.90 	Per cft	 5.14 

LABOUR CHARGE:						
	FOR LAYING PIPE	1.00 	rft	@TK	9.00 	Per rft	 9.00 
*/


INSERT INTO costlines VALUES("PI04","CIVMAT","9"" dia R.C.C pipe",20,1,"RFT");
INSERT INTO costlines VALUES("AM03","AUXMAT","One layer brick flat soling",10.0,1,"SFT");
INSERT INTO costlines VALUES("AM04","AUXMAT","C.C. 1:3:6",90.0,1, "CFT");
INSERT INTO costlines VALUES("AM05","AUXMAT","Cost of earth work",0.80,1, "CFT");
INSERT INTO costlines VALUES("AM06","AUXMAT","Back filling of trench",0.90,1, "CFT");
INSERT INTO costlines VALUES("L002","LABOUR","FOR LAYING PIPE",9.00,1.00, "RFT");

--CIV003
/*	
CIV003
basis 100 cft

BASIC COST OF MATERIALS:						 TK 
	ST02	20mm down graded Stone chips 	22.00 	Cft	@TK	200.00 	Per cft	 4,400.00 
	SN01	Local sand  (F.M.=1.2)			5.50 	Cft	@TK	16.00 	Per cft	 88.00 
	SN02	Sylhet sand (F.M.=2.5)			5.50 	Cft	@TK	41.00 	Per cft	 225.50 
	CN01	Cement (OPC)					4.50 	Bag	@TK	390.00 	Per Bag	 1,755.00 

AUXILIARY MATERIAL COST:						
	FORMWORK						 750.00 

TOOLS & PLANTS:						 80.00 

LABOUR CHARGE:						
	FOR CASTING						 250.00 
	FOR CURING						 10.00 
*/


INSERT INTO costlines VALUES("ST02","CIVMAT","20mm down graded Stone chips",200.00,1, "CFT");
INSERT INTO costlines VALUES("SN01","CIVMAT","Local sand  F.M.=1.2",16.00,1, "CFT");
INSERT INTO costlines VALUES("SN02","CIVMAT","Sylhet sand F.M.=2.5",41.00,1, "CFT");
INSERT INTO costlines VALUES("CN01","CIVMAT","Cement OPC",390.0,1, "BAG");

INSERT INTO costlines VALUES("AM07","AUXMAT","FORMWORK for RCC Foundation",750.00,100,"CFT");
INSERT INTO costlines VALUES("AM08","TOOLS","Tools and plants for rcc foundation",80.00,100,"CFT");
INSERT INTO costlines VALUES("L004","LABOUR","Labour for casting RCC Foundation",250.00,100,"CFT");
INSERT INTO costlines VALUES("L005","LABOUR","Labour for curing RCC Foundation",10.00,100,"CFT");

select * from costlines;



-- assemblies
DROP TABLE IF EXISTS assemblies;
CREATE TABLE IF NOT EXISTS 
assemblies 
( 	
	assembly_id VARCHAR(10) PRIMARY KEY,
	item_id VARCHAR(10) NOT NULL,
	costline_id VARCHAR(10) NOT NULL,
	assembly_quantity DECIMAL,
	FOREIGN KEY (item_id) REFERENCES items(item_id),
	FOREIGN KEY (costline_id) REFERENCES costlines(costline_id)
);

INSERT INTO assemblies VALUES("A000001","CIV001","ML02",20.00);
INSERT INTO assemblies VALUES("A000002","CIV001","ML32",00.50);
INSERT INTO assemblies VALUES("A000003","CIV001","ML31",00.50);	
INSERT INTO assemblies VALUES("A000004","CIV001","AM02",1);	
INSERT INTO assemblies VALUES("A000005","CIV001","L001",1);		

INSERT INTO assemblies VALUES("A000006","CIV002","PI04",1.00);
INSERT INTO assemblies VALUES("A000007","CIV002","AM03",2.08);
INSERT INTO assemblies VALUES("A000008","CIV002","AM04",1.13);
INSERT INTO assemblies VALUES("A000009","CIV002","AM05",7.29);
INSERT INTO assemblies VALUES("A000010","CIV002","AM06",5.71);
INSERT INTO assemblies VALUES("A000011","CIV002","L002",1.00);

INSERT INTO assemblies VALUES("A000012","CIV002","ST02",22.00);
INSERT INTO assemblies VALUES("A000013","CIV002","SN01",5.50);
INSERT INTO assemblies VALUES("A000014","CIV002","SN02",5.50);
INSERT INTO assemblies VALUES("A000015","CIV002","CN01",4.50);
INSERT INTO assemblies VALUES("A000016","CIV002","AM07",1);
INSERT INTO assemblies VALUES("A000017","CIV002","AM08",1);
INSERT INTO assemblies VALUES("A000018","CIV002","L004",1);
INSERT INTO assemblies VALUES("A000019","CIV002","L005",1);

select * from assemblies;