import os
import openpyxl
import csv
import sys
import CSVWriter as csvw


def print_lines(Line_collection):
    print("printing")
    print("length of lines col " + str(len(Line_collection)))
    for l in range (0,len(Line_collection)):
        print(Line_collection[l])

def myMain(file_name,Lines):


    wb = openpyxl.load_workbook(file_name,data_only=True)
    ws = wb.active

    row_id = 1
    for x in range (row_id,30000,48):

        Line_ID = ws.cell((x+4),2).value
        Description1 = ws.cell((x+5),2).value
        Description2 = ws.cell((x+6),2).value

        if Description2:
            Line_Description = Description2 + Description1
        else :
            Line_Description = Description1

        Line_Basis = ws.cell((x+8),2).value

        if Line_ID:
            # print (Line_ID + " : " + Line_Description + " : " + Line_Basis)

            # Material Collector
            for y in range((x+11),(x+22)):
                print('x: ' + str(x) + " y: " + str(y))
                if ws.cell(y,1).value:
                    Line = {}
                    Line = {"Line_ID": Line_ID, "DESCRIPTION": Description1 + Line_Description, "Line_Basis": Line_Basis}
                    Line["Line_Cost_ID"] = ws.cell(y,1).value
                    Line["Line_Cost_Type"] = "MATERIAL"
                    Line["Line_Cost_Description"] = ws.cell(y,2).value
                    Line["Line_Cost_Quantity"] = ws.cell(y,3).value
                    Line["Line_Cost_Quantity_Unit"] = ws.cell(y,4).value
                    Line["Line_Cost_Cost"] = ws.cell(y,6).value
                    Line["Line_Cost_Cost_Unit"] = ws.cell(y,7).value
                    Line["Line_Cost_Amount"] = ws.cell(y,8).value

                    Lines.append(Line)


                else:
                    print("Breaking out of Material")
                    break

            # Auxilary Material Collector 26 to 30
            for y in range((x+24),(x+29)):
                print('Auxiliary x: ' + str(x) + " y: " + str(y))
                if ws.cell(y,2).value:
                    Line = {}
                    Line = {"Line_ID": Line_ID, "DESCRIPTION": Description1 + Line_Description, "Line_Basis": Line_Basis}
                    Line["Line_Cost_ID"] = (ws.cell(y,1).value)
                    Line["Line_Cost_Type"] = "AUXULIARYMATERIAL"
                    Line["Line_Cost_Description"] = ws.cell(y,2).value
                    Line["Line_Cost_Quantity"] = ws.cell(y,3).value
                    Line["Line_Cost_Quantity_Unit"] = ws.cell(y,4).value
                    Line["Line_Cost_Cost"] = ws.cell(y,6).value
                    Line["Line_Cost_Cost_Unit"] = ws.cell(y,7).value
                    Line["Line_Cost_Amount"] = ws.cell(y,8).value

                    Lines.append(Line)


                else:
                    print("Breaking out of Aux")
                    break

            # LOCAL CARTAGE AND SUNDRIES 31
            for y in range((x+30),(x+31)):
                print('LOCAL CARTAGE x: ' + str(x) + " y: " + str(y))
                if ws.cell(y,8).value:
                    Line = {}
                    Line = {"Line_ID": Line_ID, "DESCRIPTION": Description1 + Line_Description, "Line_Basis": Line_Basis}
                    Line["Line_Cost_ID"] = (ws.cell(y,1).value)
                    Line["Line_Cost_Type"] = "LOCALCARTAGEANDSUNDRIES"
                    Line["Line_Cost_Description"] = ws.cell(y,2).value
                    Line["Line_Cost_Quantity"] = ws.cell(y,3).value
                    Line["Line_Cost_Quantity_Unit"] = ws.cell(y,4).value
                    Line["Line_Cost_Cost"] = ws.cell(y,6).value
                    Line["Line_Cost_Cost_Unit"] = ws.cell(y,7).value
                    Line["Line_Cost_Amount"] = ws.cell(y,8).value

                    Lines.append(Line)


                else:
                    print("Breaking ouT Local Cartage")
                    break

            #Tools and Plants
            for y in range((x+31),(x+32)):
                if ws.cell(y,8).value:
                    Line = {}
                    Line = {"Line_ID": Line_ID, "DESCRIPTION": Description1 + Line_Description, "Line_Basis": Line_Basis}
                    Line["Line_Cost_ID"] = (ws.cell(y,1).value)
                    Line["Line_Cost_Type"] = "TOOLS"
                    Line["Line_Cost_Description"] = ws.cell(y,2).value
                    Line["Line_Cost_Quantity"] = ws.cell(y,3).value
                    Line["Line_Cost_Quantity_Unit"] = ws.cell(y,4).value
                    Line["Line_Cost_Cost"] = ws.cell(y,6).value
                    Line["Line_Cost_Cost_Unit"] = ws.cell(y,7).value
                    Line["Line_Cost_Amount"] = ws.cell(y,8).value

                    Lines.append(Line)
                else:
                    print("Breaking out of tools and plants")
                    break

            # 35 TO 40 LABOUR
            for y in range((x+34),(x+39)):
                print('LABOUR x: ' + str(x) + " y: " + str(y))
                if ws.cell(y,2).value:
                    print("inside loop")
                    Line = {}
                    Line = {"Line_ID": Line_ID, "DESCRIPTION": Description1 + Line_Description, "Line_Basis": Line_Basis}
                    Line["Line_Cost_ID"] = (ws.cell(y,1).value)
                    Line["Line_Cost_Type"] = "LABOUR"
                    Line["Line_Cost_Description"] = ws.cell(y,2).value
                    Line["Line_Cost_Quantity"] = ws.cell(y,3).value
                    Line["Line_Cost_Quantity_Unit"] = ws.cell(y,4).value
                    Line["Line_Cost_Cost"] = ws.cell(y,6).value
                    Line["Line_Cost_Cost_Unit"] = ws.cell(y,7).value
                    Line["Line_Cost_Amount"] = ws.cell(y,8).value

                    Lines.append(Line)


                else:
                    print("Breaking ouT Local Cartage")
                    break

        else:
            print("Breaking")
            break

    print(len(Lines))

def writeToCSV(LinesDict):
    csvw.WriteDictToCSV(
        csv_file=file_name_output_csv,
        csv_columns=file_headers,
        dict_data=Lines
    )


if __name__ == "__main__":

    file_name_output_csv= "data/civ.csv"
    Lines = []

    file_headers = ["Line_ID",
                    "DESCRIPTION",
                    'Line_Basis',
                    'Line_Cost_ID',
                    'Line_Cost_Type',
                    'Line_Cost_Description',
                    'Line_Cost_Quantity',
                    'Line_Cost_Quantity_Unit',
                    'Line_Cost_Cost',
                    'Line_Cost_Cost_Unit',

                    'Line_Cost_Amount'
                    ]
    for file_name in ("data/CIV_FR01.XLSX","data/CIV_FR02.XLSX","data/CIV_FR03.XLSX","data/CIV_FR04.XLSX",
                      "data/CIV_FR05.XLSX","data/CIV_FR06.XLSX","data/CIV_FR07.XLSX","data/CIV_FR08.XLSX",):

        myMain(file_name,Lines)
        print_lines(Lines)
        writeToCSV(Lines)


