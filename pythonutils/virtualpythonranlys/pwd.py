import openpyxl
import openpyxl.worksheet

def readXL():
    file_name="data\pwd.xlsx"
    wb = openpyxl.load_workbook(file_name, data_only=True)
    print(wb.sheetnames)

    ws = wb.get_sheet_by_name("BREAK-UP")
    for x in range(1,1600,1):
        cellItemNoValue = ws.cell(x,1).value
        if cellItemNoValue is not None:
            if "Item No." in cellItemNoValue:
                print(cellItemNoValue + " " + str(ws.cell(x,1).row))


if __name__ == "__main__":
    readXL()